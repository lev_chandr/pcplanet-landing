$(document).ready(function () {
    $("body").on("click", ".js-show-more", function() {
        $(this).toggleClass("active").siblings(".card-params-hidden").toggle();
        return false;
    });

    $("body").on("click", ".js-popup-buy", function() {
        var attr = $(this).attr("data-popup");
        $("body, header, .header-top-line").addClass("popup-open");
        $(".one-click__popup").show().find(".popup__item[data-popup=" + attr + "]").show();
        setPopupPosition();
        return false;
    });
    $("body").on("click", ".js-close__btn", function() {
        $("body, header, .header-top-line").removeClass("popup-open");
        $(this).closest(".popup__item").hide().closest(".one-click__popup").hide();
        return false;
    });

    $("body").on("click", ".one-click__popup", function(e) {
        if (!$(".popup__item").is(e.target) && $(".popup__item").has(e.target).length === 0) {
            $("body, header, .header-top-line").removeClass("popup-open");
            $(".popup__item").hide().closest(".one-click__popup").hide();
        }
    });

    setPopupPosition();
    $(window).resize(function () {
        setPopupPosition();
    });
});


function setPopupPosition() {
    $(".popup__item").each(function() {
        var coordY = $(window).height()/2 - $(this).innerHeight()/2;
        if(coordY < 0) {
            coordY = 0;
        }
        $(this).offset({top:coordY});
    });
}