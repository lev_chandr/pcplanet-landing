$(document).ready(function () {
    $("body").on("click", ".popup__item .js-minus", function() {
       var input = $(this).siblings(".js__product_counter_slider_container").find("input");
       var inputVal = +input.val();
       if(inputVal < 2) {
           return false;
       }
        input.val(inputVal - 1);
       return false;
    });

    $("body").on("click", ".popup__item .js-plus", function() {
        var input = $(this).siblings(".js__product_counter_slider_container").find("input");
        input.val(+input.val() + 1);
       return false;
    });
});